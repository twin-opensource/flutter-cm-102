/////[dashboar.dart],is another [Widget] which return the whole page.
///[Stateless widget]doesn't has [initial state], all [widget] and [variable] is [const]

import 'package:flutter/material.dart';

class DashBoard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ////[Class MediaQueryData] will get the [User's mobile information]
    MediaQueryData queryData = MediaQuery.of(context);

    ////get data of [Screen width/heigh]
    double screenWidth = queryData.size.width;
    double screenHeight = queryData.size.height;

    ////[Method][Generate]the list of widget from your data source.
    List<Widget> myList() {
      List<Widget> myList = List<Widget>();
      for (var i = 0; i < 20; i++) {
        myList.add(Container(
            margin: new EdgeInsets.all(screenHeight * 0.05),
            width: screenWidth,
            height: screenHeight * 0.3,
            color: Colors.amber,
            alignment: AlignmentDirectional.center,
            child: Text("$i")));
      }
      return myList;
    }

    ///// Retrun this [dashboard.dart][Stateless widget][content]
    return new Scaffold(
        appBar: new AppBar(
            title: new Text("Dashboard"),
            backgroundColor: Colors.blueGrey.shade900),
        backgroundColor: Colors.grey.shade400,
        body: ListView(children: myList()));
  }
}
