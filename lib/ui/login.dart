/////The [First Widget], which return the whole page, when the [main.dart] was called

import 'package:flutter/material.dart';
import 'dashboard.dart';

class Login extends StatefulWidget {
  ////made as [Statefulwidget] because this widget will be [redraw] when got user interactice.
  @override
  State<StatefulWidget> createState() {
    return new MakeLoginState();
  }
}

class MakeLoginState extends State<Login> {
////Class Controller will act likes [Personal AI] for each [widget].
  ///Refer to [_userController] and [_passwordController],it acts likes [AI] for input [Input TextField] that wil controller the [output] and [input] only itslef.
  final TextEditingController _userController = new TextEditingController();
  final TextEditingController _passwordController = new TextEditingController();

////[Method]
  void _loginPressed() {
    setState(() {
      if (_userController.text.isNotEmpty &&
          _passwordController.text.isNotEmpty) {
        ////Method [Go to anoter screen]
        Navigator.push(context,
            new MaterialPageRoute(builder: (context) => new DashBoard()));
      } else {
        ////Create [Obj] that containts [Dialog context], called [alert]
        var alert = new AlertDialog(
            title: new Text("Sign in Error"),
            content:
                new Text("There was an error signing in. Please try again."));
        ////Callled method [showDialog],with [child] is [alert]
        showDialog(context: context, builder: (context) => alert);
      }
    });
  }

  void _cancelPressed() {
    setState(() {
      _userController.clear();
      _passwordController.clear();
    });
  }

  checkValidate() {
    ///At this [current moment], check [text] inseide [_userController] is [null or not].
    if (_userController.text.isEmpty) {
      setState(() {
        checkEmpty = true;
      });
    } else {
      setState(() {
        checkEmpty = false;
      });
    }
  }

  @override
  void initState() {
    //// At [initial state] we assign the [_usernameController] to [listen / focus] on print [checkValidate] method.
    _userController.addListener(checkValidate);
    super.initState();
  }

  ////[Variable] that check [username input] is [empty] or [not]
  bool checkEmpty = true;

  @override
  Widget build(BuildContext context) {
    ////Create [Widget] can be [Widget myWidget] or using that Widget name [Text myText]
    ///

    Image logoImage = Image.asset("images/logotwin.png",
        width: 200.0, height: 200.0, color: Colors.blueGrey.shade500);

    TextField userInput = TextField(
        controller: _userController,
        keyboardType: TextInputType.emailAddress,
        decoration: new InputDecoration(
            hintText: "EMAIL", icon: new Icon(Icons.email)));

    TextField passwordInput = TextField(
        controller: _passwordController,
        decoration: new InputDecoration(
            hintText: "PASSWORD", icon: new Icon(Icons.security)),
        obscureText: true);

    Widget btnFormSubmit = Center(
        child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
          new FlatButton(
              color: checkEmpty ? Colors.blueAccent.shade200 : Colors.red,
              textColor: Colors.white70,
              onPressed: _loginPressed,
              child: new Text("LOGIN")),
          new Padding(padding: new EdgeInsets.all(15.0)),
          new FlatButton(
              color: Colors.red.shade200,
              textColor: Colors.white70,
              onPressed: _cancelPressed,
              child: new Text("CANCEL")),
        ]));

    Widget txtCopyRight = Center(
        child: new Text("Copy Right 2018 by Twinsynergy Co., Ltd.",
            style: new TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w300,
                fontSize: 14.0)));

    Widget content = new Container(
        alignment: Alignment.topCenter,
        padding: new EdgeInsets.all(20.0),
        child: new ListView(children: <Widget>[
          logoImage,
          new Container(
              height: 200.0,
              width: 380.0,
              padding: new EdgeInsets.all(20.0),
              color: Colors.grey.shade50,
              child: new Column(
                  children: <Widget>[userInput, passwordInput, btnFormSubmit])),
          txtCopyRight
        ]));

    ///// Retrun this [Login.dart][Stateful widget][content]
    return new Scaffold(
        appBar: new AppBar(
            title: new Text("LOGIN"),
            backgroundColor: Colors.blueGrey.shade900),
        backgroundColor: Colors.blueGrey.shade100,
        body: content);
  }
}
